# Example Static Code Analysis Configuration and Code Beutification

![forthebadge](https://forthebadge.com/images/badges/uses-html.svg) 
![forthebadge](https://forthebadge.com/images/badges/uses-js.svg) 
![Bitbucket Pipelines](https://img.shields.io/bitbucket/pipelines/Skerwe/web-static-code-analysis-boilerplate?style=for-the-badge)

Boilerplate static code analysis and formatting configuration for a web project.

- HTML linting
- Style linting (CSS, SASS)
- HTML, styles and JavaScript formatting

## Static Code Analysis

### [HTMLHint](https://github.com/htmlhint/HTMLHint)

`npm install htmlhint --save-dev`

### [Stylelint](https://stylelint.io/)

`npm install stylelint --save-dev`

## Code Formatter

[EditorConfig](https://editorconfig.org/) helps maintain consistent coding styles.

### [Prettier](https://prettier.io/)

[Prettier on GitHub](https://github.com/prettier/prettier)

`npm install --save-dev --save-exact prettier`

`npm install --save-dev stylelint-config-prettier`  
`npm install --save-dev stylelint-prettier`

## Validators

- [W3C Markup Validation Service](https://validator.w3.org/)
- [W3C CSS Validation Service](https://jigsaw.w3.org/css-validator/)
